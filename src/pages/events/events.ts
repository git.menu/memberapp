import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { EventDetailsPage } from '../event-details/event-details';
import {HttpProvider } from '../../providers/http/http';
import {  SharedService } from '../../providers/global';
import { LocalStorageService } from 'angular-2-local-storage';

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  Events:any;
  event_list:any =[];
  Eventlist:any;
  EventName:any;
  Name:any;
  keys:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public httpProvider: HttpProvider,public globalVar:SharedService,private localStorageService: LocalStorageService) {
  }
  
  getEventList () {
    this.httpProvider.getEvents().subscribe(data=>{
      console.log(data);
      this.Events=data;
      //this.Event=this.Events.results[0].description;
      this.Eventlist=this.Events.results;
     console.log(this.Eventlist);
     this.Name=this.Events.results.value;
     console.log(this.Name);
      this.keys= Object.keys(this.Eventlist);
      console.log(this.keys);
     this.localStorageService.set('EventName', this.Name);

    });
  }


  goToEventDetail(Event){
  this.httpProvider.setItem('Name',Event['value'])
  this.navCtrl.push(EventDetailsPage,{Name:Event['value']});
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
    this.getEventList();
  }

}
