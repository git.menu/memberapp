import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { EventsPage } from '../events/events';
import { HttpProvider } from '../../providers/http/http';
import {  SharedService } from '../../providers/global';
import { LocalStorageService } from 'angular-2-local-storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
  

})
export class LoginPage {


  //declaring and assigning values for URL,username,password
  URL:any="";
   data = {
        username: this.httpProvider.getItem("user_name"),
        password: this.httpProvider.getItem("pass_word"),
    };
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider: HttpProvider,public globalVar:SharedService,public alertCtrl: AlertController, public loadingCtrl: LoadingController, private localStorageService: LocalStorageService) {
   this.URL = this.globalVar.URL;
    //this.localStorageService.set('this.username",data.username)
    //this.localStorageService.set('this.password",data.password)

  }

 


    //defining login function
    goToLogin(){
      this.globalVar.URL = this.URL;

      var data = {
        username:this.data.username,
        password:this.data.password

      }
      console.log(data);
      let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 1500
          });
    loader.present();
      //calling function in servicelayer and passing data as param
      this.httpProvider.login(data).subscribe(data => {
      
           let result=data;
          console.log(result);
          console.log(result['message']);
          console.log(result['full_name']);
          let Name=result['full_name'];
          console.log(Name);
          let Message =result['message'];
          console.log(Message);
           this.localStorageService.set('name',Name);
          if (Message =="Logged In") {
          //navigating to homepage
          this.navCtrl.push(EventsPage);
          }
         else{
       let alert = this.alertCtrl.create({
      title: 'Sorry!',
      subTitle: 'Login Failed due to invalid credentials',
      buttons: ['OK']
      });
      alert.present();
      }
    },(err)=>{
      let alert = this.alertCtrl.create({
      title: 'Error!',
      // subTitle: err,
      subTitle : "Invalid Credentials",
      buttons: ['OK']
    });
    alert.present();
        console.log(err);
        return err;
      })
    
    
}









  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
