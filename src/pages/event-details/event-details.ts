import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {HttpProvider } from '../../providers/http/http';
import {  SharedService } from '../../providers/global';
import { LocalStorageService } from 'angular-2-local-storage';

import { HomePage } from '../home/home';
import { DonatePage } from '../donate/donate';

/**
 * Generated class for the EventDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-details',
  templateUrl: 'event-details.html',
})
export class EventDetailsPage {

   Description:any; 
    Image:any;
    selectedEvent="";
    BASE_URL="";
  constructor(public navCtrl: NavController, public navParams: NavParams,public httpProvider: HttpProvider,public globalVar:SharedService,private localStorageService: LocalStorageService) {

  this.BASE_URL=this.globalVar.URL;

  this.selectedEvent=this.httpProvider.getItem('Name')
  console.log( this.selectedEvent);


  }



  getImage() {
    console.log(this.selectedEvent)
    this.httpProvider.getEventImage(this.selectedEvent).subscribe(data=>{
      console.log(data);
      this.Image=data['fetch_values'][0];
      console.log( this.Image);
      
    });
     this.getDescription();
  }

  getDescription() {
    console.log(this.selectedEvent)
    this.httpProvider.getEventDescription(this.selectedEvent).subscribe(data=>{
      console.log(data);
      this.Description=data['fetch_values'][0];
         console.log( this.Description);
    });
  }










   goToQrcode(){
       this.navCtrl.push(HomePage);
  }
  goToDonate(){
       this.navCtrl.push(DonatePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventDetailsPage');
     this.getImage();
  }

}
