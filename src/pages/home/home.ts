import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import QRCode from 'qrcode';

import { LoginPage } from '../login/login';
import {HttpProvider } from '../../providers/http/http';
import {  SharedService } from '../../providers/global';
import { LocalStorageService } from 'angular-2-local-storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
   providers: [ HttpProvider]
})
export class HomePage {

  id:any;
  QRCode:any;
  code:any;
  NAME:any;
  ID:any;
  generated = '';
  result:any;

  displayQrCode() {
    return this.generated !== '';
  }


  constructor(public navCtrl: NavController,public httpProvider: HttpProvider,public globalVar:SharedService,private localStorageService: LocalStorageService) {
    this.NAME=this.localStorageService.get('name')
    console.log(this.NAME);
  }

  process() {
      const qrcode = QRCode;
    const self = this;
    qrcode.toDataURL(self.code, { errorCorrectionLevel: 'H' }, function (err, url) {
      self.generated = url;
    })
  }


  getMemberList(){

   this.httpProvider.getMember().subscribe(data => {
    console.log(data);
    this.result=data;
     this.ID = this.result.data[0].name
     var text = {
      name:this.NAME,
      id:this.ID
  } 
    this.code=JSON.stringify(text);
  });
   
  }

   ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  this.getMemberList()
  }
}





