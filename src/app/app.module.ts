import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { LocalStorageModule } from 'angular-2-local-storage';



import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EventsPage } from '../pages/events/events';
import { EventDetailsPage } from '../pages/event-details/event-details';
import { DonatePage } from '../pages/donate/donate';
import { LoginPage } from '../pages/login/login';
import { HttpProvider } from '../providers/http/http';
import { SharedService } from '../providers/global';
import { MainhttpProvider } from '../providers/mainhttp/mainhttp';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EventsPage,
    EventDetailsPage,
    DonatePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    LocalStorageModule.withConfig({
        prefix: 'my-app',
        storageType: 'localStorage'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    EventsPage,
    EventDetailsPage,
    DonatePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HttpProvider,
    SharedService,
    MainhttpProvider
  ]
})
export class AppModule { }
