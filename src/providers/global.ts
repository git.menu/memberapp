import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
  URL:string = "http://40.76.21.162:8002/"; 
  login_url = "api/method/login";
  member_url = "api/resource/Member";


  headers = {
  	"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
  }

 time_stamp:any= new Date().getTime();

//get event list --Get

  event_list = {
  	txt: "",

	doctype: "Events",

	ignore_user_permissions: 0,

	reference_doctype: "Member Attendance",

	cmd: "frappe.desk.search.search_link",

	_: this.time_stamp

  }

//Get event Image --Get
  get_event_Image = {
    value: "EVENT-00004",

  options: "Events",

  fetch: ["image"],


  cmd: "frappe.desk.form.utils.validate_link",

  _: this.time_stamp
  }

//Get event description --Get
  get_event_Description = {
    value: "EVENT-00004",

  options: "Events",

  fetch: ["discription","image"],


  cmd: "frappe.desk.form.utils.validate_link",

  _: this.time_stamp
  }





}