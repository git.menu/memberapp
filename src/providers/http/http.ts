import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {SharedService} from '../global';
import { MainhttpProvider } from '../mainhttp/mainhttp';
import { LocalStorageService } from 'angular-2-local-storage';

/*
  Generated class for the HttpProvider provider.  

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {


    data:any;
    code:any;

  constructor(public http: HttpClient,public globalVar:SharedService,public mainHttp:MainhttpProvider ) {
    console.log('Hello HttpProvider Provider');
      localStorage.removeItem('user_name')
      localStorage.removeItem('password')
  }

  
//defining login function to hit login API
  login (data){
  //constructing req_data object and using url from global file
    var req_data = {
      "url":this.globalVar.login_url,
      "headers":{
        "Content-type":"application/json"
      },
      "method":"GET",
      "params":{
        usr:data.username,
        pwd:data.password
      }
    }
    //calling API from mainhttp service by passing req_data as param 
    return this.mainHttp.api_call(req_data)
  
  }
   getMember(){ 
     var req_data={
     "url":this.globalVar.member_url,
      "headers":{
        "Content-type":"application/json"
      },
      "method":"GET",
      "params":""
      }
      return this.mainHttp.api_call(req_data)
     }

 //defining getEvents function
  getEvents(){
    //constructing req_data object and using params from global file
    var req_data = {
      "url":"",
      "method":"GET",
      "params":this.globalVar.event_list
    }
      //returning calling API from mainhttp service by passing req_data as param
     return this.mainHttp.api_call(req_data)
  }



getEventImage(event){
    this.globalVar.get_event_Image.value = event;
    var req_data = {
      "url":"",
      "method":"GET",
      "params":this.globalVar.get_event_Image
    }
       //returning and calling API from mainhttp service by passing req_data as param
    return this.mainHttp.api_call(req_data)
  }


getEventDescription(event){
    this.globalVar.get_event_Description.value = event;
    var req_data = {
      "url":"",
      "method":"GET",
      "params":this.globalVar.get_event_Description
    }
       //returning and calling API from mainhttp service by passing req_data as param
    return this.mainHttp.api_call(req_data)
  }









  setItem(key, value) {
        // to store the data in our local storage

        localStorage.setItem(key, value);
    
    };

  getItem(key) {
      
        return localStorage.getItem(key);
    
    };








  
objectToFormData = function(obj, form?, namespace?) {
    
   const searchParams = Object.keys(obj).map(key => {
        if(key == 'doc'){
            return key + '=' + JSON.stringify(obj[key]);
        }else{
            return key + '=' + obj[key];
        }

      }).join('&');

   return searchParams
  };



}
